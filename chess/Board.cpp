#include "Board.h"
Board::Board()
{
	_gameBoard[0][0] = 'R';
	_gameBoard[0][1] = 'N';
	_gameBoard[0][2] = 'B';
	_gameBoard[0][3] = 'K';
	_gameBoard[0][4] = 'Q';
	_gameBoard[0][5] = 'B';
	_gameBoard[0][6] = 'N';
	_gameBoard[0][7] = 'R';
	for (int i = 0; i < BOARD_SIZE; i++)
		_gameBoard[1][i] = 'P';
	for (int i = 2; i < BOARD_SIZE - 2; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
			_gameBoard[i][j] = '#';
	}
	for (int i = 0; i < BOARD_SIZE; i++)
		_gameBoard[6][i] = 'p';
	_gameBoard[7][0] = 'r';
	_gameBoard[7][1] = 'n';
	_gameBoard[7][2] = 'b';
	_gameBoard[7][3] = 'k';
	_gameBoard[7][4] = 'q';
	_gameBoard[7][5] = 'b';
	_gameBoard[7][6] = 'n';
	_gameBoard[7][7] = 'r';
}

void Board::Print()const
{
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
			std::cout << _gameBoard[i][j] << " ";
		std::cout << "\n" << std::endl;
	}
}

void Board::getStartString (char* dest)
{
	std::string temp = "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0";
	
	for (int i = 0; i < temp.length; i++)
	{
		dest[i] = temp[i];
	}
}

std::string Board::Update(std::string newMove, bool currColor)
{
	const int ASCII_OFFSET = 96;
	char piece = '#';
	point startPos;
	point destPos;

	startPos.setX ((int)newMove[0] - ASCII_OFFSET);
	startPos.setY ((int)newMove[1] - ASCII_OFFSET);
	destPos.setX ((int)newMove[2] - ASCII_OFFSET);
	destPos.setY ((int)newMove[3] - ASCII_OFFSET);

	if( ( 0 > startPos.getX () || startPos.getX () > 7 ) || (0 > startPos.getY () || startPos.getY () > 7) || (0 > destPos.getX () || destPos.getX () > 7) || (0 > destPos.getY () || destPos.getY () > 7))
		// return code 5

	if( startPos.getX() == destPos.getX() && startPos.getY() == destPos.getY())
		//return code 7
	
	piece = _gameBoard[(int)newMove[0] - ASCII_OFFSET][(int)newMove[1] - ASCII_OFFSET];

	switch (piece)
	{
	r:
	R:
		//rook
		break;

	n:
	N:
		//knight
		break;

	b:
	B:
		//bishop
		break;

	k:
	K:
		//king
		break;

	q:
	Q:
		//queen
		break;

	p:
	P:
		//pawn
		break;

	default:
			//return code 2
		break;
	}
}

char Board::getTile (int x, int y)
{
	//Y is the row and X is the column
	return _gameBoard[y][x];
}