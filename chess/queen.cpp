#include "queen.h"
#include "Board.h"
const int ASCII_b = 98;
const int ASCII_k = 107;
const int ASCII_r = 114;
const int ASCII_B = 66;
const int ASCII_K = 75;
const int ASCII_R = 83;

queen::queen(bool color) : _color{color}
{ }


int queen::move (point start, point dest)
{
	std::string* compMoves;
	compMoves = get_all_moves (start);

	Board board;
	point compDest;//var that holds tiles from the list of potential move locations

	//guess just check it by 3^2 for x in y

	//white
	if (_color)
	{
		if (ASCII_B <= (int)board.getTile (start.getX (), start.getY ()) && (int)board.getTile (start.getX (), start.getY () <= ASCII_R))//check if start tile holds a player piece
		{
			for (int i = 0; i < compMoves->length (); i += 2)
			{
				compDest.setX ((int)((*compMoves)[i] - 48));
				compDest.setY ((int)((*compMoves)[i + 1] - 48));//setting potential tile

				if (compDest.getY () > start.getY ())// if the move is above the start on the board
				{
					if (compDest.getX () > start.getX ())// if the move is to the right of the start
					{
						for (int j = start.getY (), jj = start.getX(); j >= 0 && jj < 8; j--, jj++)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r)))
							{
								if (board.getTile (jj, j) == ASCII_k)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && ( ASCII_B <= board.getTile(jj, j) && board.getTile(jj, j) <= ASCII_R))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}

					else if (compDest.getX () < start.getX ())// if the move is to the left
					{
						for (int j = start.getY (), jj = start.getX (); j >= 0 && jj >= 0; j--, jj--)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r)))
							{
								if (board.getTile (jj, j) == ASCII_k)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}

					else// if the move is in the middle
					{
						for (int j = start.getY (), jj = start.getX (); j >= 0 ; j--)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r)))
							{
								if (board.getTile (jj, j) == ASCII_k)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}
				}

				else if (compDest.getY () < start.getY ())// if the move is below
				{
					if (compDest.getX () > start.getX ())// if the move is to the right of the start
					{
						for (int j = start.getY (), jj = start.getX (); j < 8 && jj < 8; j++, jj++)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r)))
							{
								if (board.getTile (jj, j) == ASCII_k)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}

					else if (compDest.getX () < start.getX ())// if the move is to the left
					{
						for (int j = start.getY (), jj = start.getX (); j < 8 && jj >= 0; j++, jj--)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r)))
							{
								if (board.getTile (jj, j) == ASCII_k)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}

					else// if the move is in the middle
					{
						for (int j = start.getY (), jj = start.getX (); j < 8; j++)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r)))
							{
								if (board.getTile (jj, j) == ASCII_k)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}
				}

				else// if the move is level with the start location
				{
					if (compDest.getX () > start.getX ())// if the move is to the right of the start
					{
						for (int j = start.getY (), jj = start.getX (); jj < 8; jj++)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r)))
							{
								if (board.getTile (jj, j) == ASCII_k)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}

					else if (compDest.getX () < start.getX ())// if the move is to the left
					{
						for (int j = start.getY (), jj = start.getX (); jj >= 0; jj--)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r)))
							{
								if (board.getTile (jj, j) == ASCII_k)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}
				}
			}
		}
		else
		{
			delete compMoves;
			return 2;
		}
	}

	//black
	else
	{
		if (ASCII_b <= (int)board.getTile (start.getX (), start.getY ()) && (int)board.getTile (start.getX (), start.getY () <= ASCII_r))
		{
			for (int i = 0; i < compMoves->length (); i += 2)
			{
				compDest.setX ((int)((*compMoves)[i] - 48));
				compDest.setY ((int)((*compMoves)[i + 1] - 48));

				if (compDest.getY () > start.getY ())
				{
					if (compDest.getX () > start.getX ())
					{
						for (int j = start.getY (), jj = start.getX (); j >= 0 && jj < 8; j--, jj++)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R)))
							{
								if (board.getTile (jj, j) == ASCII_K)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}

					else if (compDest.getX () < start.getX ())
					{
						for (int j = start.getY (), jj = start.getX (); j >= 0 && jj >= 0; j--, jj--)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R)))
							{
								if (board.getTile (jj, j) == ASCII_K)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}

					else
					{
						for (int j = start.getY (), jj = start.getX (); j >= 0; j--)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R)))
							{
								if (board.getTile (jj, j) == ASCII_K)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}
				}

				else if (compDest.getY () < start.getY ())
				{
					if (compDest.getX () > start.getX ())
					{
						for (int j = start.getY (), jj = start.getX (); j < 8 && jj < 8; j++, jj++)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R)))
							{
								if (board.getTile (jj, j) == ASCII_K)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}

					else if (compDest.getX () < start.getX ())
					{
						for (int j = start.getY (), jj = start.getX (); j < 8 && jj >= 0; j++, jj--)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R)))
							{
								if (board.getTile (jj, j) == ASCII_K)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}

					else
					{
						for (int j = start.getY (), jj = start.getX (); j < 8; j++)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R)))
							{
								if (board.getTile (jj, j) == ASCII_K)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}
				}

				else
				{
					if (compDest.getX () > start.getX ())
					{
						for (int j = start.getY (), jj = start.getX (); jj < 8; jj++)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R)))
							{
								if (board.getTile (jj, j) == ASCII_K)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}

					else if (compDest.getX () < start.getX ())
					{
						for (int j = start.getY (), jj = start.getX (); jj >= 0; jj--)
						{
							if (dest.getY () == j && dest.getX () == jj && (board.getTile (jj, j) == '#' || (ASCII_B <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_R)))
							{
								if (board.getTile (jj, j) == ASCII_K)
								{
									delete compMoves;
									return 1;
								}
								delete compMoves;
								return 0;
							}
							else if (dest.getY () == j && dest.getX () == jj && (ASCII_b <= board.getTile (jj, j) && board.getTile (jj, j) <= ASCII_r))
							{
								delete compMoves;
								return 3;
							}
							else if (board.getTile (jj, j) != '#')
								break;
						}
					}
				}
			}
		}
		else
		{
			delete compMoves;
			return 2;
		}
	}

	delete compMoves;
	return 6;
}

std::string* queen::get_all_moves(point start)
{
	std::string* moves = new std::string;
	Board board;
	int i = 0;
	int j = 0;
	//12:00 - clock directions of where the queen can go
	try
	{
		for( i = start.getY() - 1; i >= 0; i--)
			moves->append (std::to_string (start.getX()) + std::to_string (i));
	}
	catch (...) {}
	//1:30
	try
	{
		j = start.getX () + 1;
		for (i = start.getY () - 1; i >= 0 && j < 8 ; i--, j++)
			moves->append (std::to_string (j) + std::to_string (i));
	}
	catch (...) {}
	//3:00
	try
	{
		for( j = start.getX() + 1; j < 8; j++)
			moves->append (std::to_string (j) + std::to_string (start.getY()));
	}
	catch (...) {}
	//4:30
	try
	{
		j = start.getX () + 1;
		for( i = start.getY() + 1; i < 8 && j < 8; i++, j++)
			moves->append (std::to_string (j) + std::to_string (i));
	}
	catch (...) {}
	//6:00
	try
	{
		for (i = start.getY () + 1; i < 8; i++)
			moves->append (std::to_string (start.getX ()) + std::to_string (i));
	}
	catch (...) {}
	//7:30
	try
	{
		j = start.getX () - 1;
		for (i = start.getY () + 1; i < 8 && j >= 0; i++, j--)
			moves->append (std::to_string (j) + std::to_string (i));
	}
	catch (...) {}
	//9:00
	try
	{
		for (j = start.getX () - 1; j >= 0; j--)
			moves->append (std::to_string (j) + std::to_string (start.getY ()));
	}
	catch (...) {}
	//10:30
	try
	{
		j = start.getX () - 1;
		for (i = start.getY () - 1; i >= 0 && j >= 0; i--, j--)
			moves->append (std::to_string (j) + std::to_string (i));
	}
	catch (...) {}
}