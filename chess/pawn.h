#pragma once
#include <iostream>
#include <string>
#include "troop.h"
#include "Board.h"
class pawn : public troop, public Board
{
public:
	pawn(bool color);
	virtual std::string* get_all_moves(point start, bool color);
	virtual int move(point start, point dest);
private:
	bool _color;//true for white false for black
};
