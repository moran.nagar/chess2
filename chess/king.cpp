#include "king.h"
using namespace std;
const int CORNER_OF_THE_BOARD = 1;
const int THE_SIDES_OF_THE_BOARD = 2;
const int MIDDLE_OF_THE_BOARD = 3;
king::king(bool color) : _color{ color }
{
	_check = false;
}

std::string* king::get_all_moves(point start, bool color)
{
	string* allMoves = NULL;
	if ((start.getX() < 8 && start.getX() > -1) && (start.getY() < 8 && start.getY() > -1))
	{
		switch (edge_cases(start))
		{
		case CORNER_OF_THE_BOARD:
			if (start.getX() == 0 && start.getY() == 0)
			{
				allMoves->append("");
			}
			break;
		default:
			break;
		}
	}
	return allMoves;
}
int king::edge_cases(point start)
{
	if ((start.getX() == 0 && start.getY() == 0)|| (start.getX() == 0 && start.getY() == 7)|| (start.getX() == 7 && start.getY() == 0)|| (start.getX() == 7 && start.getY() == 7))
	{
		return CORNER_OF_THE_BOARD;
	}
	if (start.getX() == 0 || start.getY() == 0 || start.getX() == 7 || start.getY() == 7)
	{
		return THE_SIDES_OF_THE_BOARD;
	}
	return MIDDLE_OF_THE_BOARD;
}
int king::move(point start, point dest)
{
	return 0;
}
void king::set_check()
{
	if (this->_check)
	{
		this->_check = false;
	}
	this->_check = true;
}