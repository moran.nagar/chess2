#include "pawn.h"

const int ASCII_b = 98;
const int ASCII_k = 107;
const int ASCII_r = 114;
const int ASCII_B = 66;
const int ASCII_K = 75;
const int ASCII_R = 83;

pawn::pawn(bool color) : _color{color}
{ }

pawn::~pawn ()
{ }

int pawn::move (point start, point dest)
{
	std::string* compMoves;
	compMoves = get_all_moves (start, _color);

	Board board;
	point compDest;

	if (_color)
	{
		if ( ASCII_B <= (int)board.getTile (start.getX (), start.getY () )&& (int)board.getTile (start.getX (), start.getY () <= ASCII_R))
		{
			for (int i = 0; i < compMoves->length(); i += 2)
			{
				compDest.setX ((int)((*compMoves)[i] - 48));
				compDest.setY ((int)((*compMoves)[i + 1] - 48));

				if (compDest.getX () == dest.getX () && compDest.getY () == dest.getY () && start.getY () == dest.getY ())
				{
					if (ASCII_B <= board.getTile (dest.getX (), dest.getY ()) && board.getTile (dest.getX (), dest.getY ()) <= ASCII_R)
					{
						delete compMoves;
						return 3;
					}
					if (board.getTile (dest.getX (), dest.getY ()) == '#')
					{
						delete compMoves;
						return 0;
					}
				}
				else if (compDest.getX () == dest.getX () && compDest.getY () == dest.getY () && start.getY () != dest.getY ())
				{
					if (board.getTile (dest.getX (), dest.getY ()) == ASCII_k)
					{
						delete compMoves;
						return 1;
					}
					delete compMoves;
					return 0;
				}
			}
		}
		else
		{
			delete compMoves;
			return 2;
		}
	}
	else
	{
		if (ASCII_b <= (int)board.getTile (start.getX (), start.getY ()) && (int)board.getTile (start.getX (), start.getY () <= ASCII_r))
		{
			for (int i = 0; i < compMoves->length(); i += 2)
			{
				compDest.setX ((int)((*compMoves)[i] - 48));
				compDest.setY ((int)((*compMoves)[i + 1] - 48));

				if (compDest.getX () == dest.getX () && compDest.getY () == dest.getY () && start.getY () == dest.getY ())
				{
					if (ASCII_b <= board.getTile (dest.getX (), dest.getY ()) && board.getTile (dest.getX (), dest.getY ()) <= ASCII_r)
					{
						delete compMoves;
						return 3;
					}
					if (board.getTile (dest.getX (), dest.getY ()) == '#')\
					{
						delete compMoves;
						return 0;
					}
				}
				else if (compDest.getX () == dest.getX () && compDest.getY () == dest.getY () && start.getY () != dest.getY ())
				{
					if (board.getTile (dest.getX (), dest.getY ()) == ASCII_K)
					{
						delete compMoves;
						return 1;
					}
					delete compMoves;
					return 0;
				}
			}
		}
		else
		{
			delete compMoves;
			return 2;
		}
	}
	delete compMoves;
	return 6;
}

std::string* pawn::get_all_moves (point start, bool color)
{
	std::string* moves = new std::string;
	Board board;

	if (color)
	{
		if (start.getY() != 0)
		{
			moves->append (std::to_string (start.getX () ) + std::to_string (start.getY () + 1));

			if (start.getY () == 6)
			{
				moves->append (std::to_string (start.getX ()) + std::to_string (start.getY () + 2));
			}

			try
			{
				if(ASCII_b <= (int)board.getTile(start.getX() + 1, start.getY() + 1)  && (int)board.getTile (start.getX () + 1, start.getY () + 1) <= ASCII_r)
					moves->append (std::to_string (start.getX () + 1) + std::to_string (start.getY () + 1));
			}
			catch(...) { }

			try
			{
				if (ASCII_b <= (int)board.getTile (start.getX () - 1, start.getY () + 1) && (int)board.getTile (start.getX () - 1, start.getY () + 1) <= ASCII_r)
					moves->append (std::to_string (start.getX () - 1) + std::to_string (start.getY () + 1));
			}
			catch (...) { }
		}
		else
		{
			//technically the bonus but we won't be making it
			return moves;
		}
	}
	else
	{
		if (start.getY () != 7)
		{
			moves->append (std::to_string (start.getX () ) + std::to_string (start.getY () - 1));

			if (start.getY () == 1)
			{
				moves->append (std::to_string (start.getX ()) + std::to_string (start.getY () - 2));
			}

			try
			{
				if (ASCII_B <= (int)board.getTile (start.getX () + 1, start.getY () - 1) && (int)board.getTile (start.getX () + 1, start.getY () - 1) <= ASCII_R)
					moves->append (std::to_string (start.getX () + 1) + std::to_string (start.getY () - 1));
			}
			catch (...) {}

			try
			{
				if (ASCII_B <= (int)board.getTile (start.getX () - 1, start.getY () - 1) && (int)board.getTile (start.getX () - 1, start.getY () - 1) <= ASCII_R)
					moves->append (std::to_string (start.getX () - 1) + std::to_string (start.getY () - 1));
			}
			catch (...) {}
		}
		else
		{
			return moves;
		}
	}

	return moves;
}