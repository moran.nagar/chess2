#pragma once
#include <iostream>
#include <string>
#include "point.h"
class troop
{
public:
		troop() {};
		~troop() {};
		troop(bool color);
		void removeTroop();
		virtual std::string* get_all_moves(point start, bool color) = 0;
		virtual int move(point start, point dest) = 0;

		void setColor (bool color);
private:
	bool _color;//true for white false for black
};