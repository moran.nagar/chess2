#pragma once
#include <iostream>
#include <string>
class point
{
public:
		point();
		~point();
		point(int x, int y);
		void setX(const int x);
		void setY(const int y);
		int getX() const;
		int getY() const;
private:
		int _x;
		int _y;
};