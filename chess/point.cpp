#include "point.h"

point::point()
{
}
point::~point()
{
}
point::point(int x, int y) :
	_x(x),
    _y(y)
{
}
void point::setX(const int x)
{
	this->_x = x;
}
void point::setY(const int y)
{
	this->_y = y;
}
int point::getX() const
{
	return _x;
}
int point::getY() const
{
	return _y;
}