#pragma once
#include <iostream>
#include <string>
#include "troop.h"
class king
{
private:
	bool _check;
	bool _color;
	int edge_cases(point start);
public:
	virtual std::string* get_all_moves(point start, bool color);
	virtual int move(point start, point dest);
	void set_check();
	king(bool color);
};
