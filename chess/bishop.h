#pragma once
#include <iostream>
#include <string>
#include "troop.h"

class bishop
{
public:
	bishop(bool color);
	virtual std::string* get_all_moves(point start);
	virtual int move(point start, point dest);
private:
	bool _color;//true for white false for black
};