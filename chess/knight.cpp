#include "knight.h"
using namespace std;

const int ASCII_b = 98;
const int ASCII_k = 107;
const int ASCII_r = 114;
const int ASCII_B = 66;
const int ASCII_K = 75;
const int ASCII_R = 83;

knight::knight(bool color) : _color{ color }
{

}

int knight::move (point start, point dest)
{
	std::string* compMoves;
	compMoves = get_all_moves (start);

	Board board;
	point compDest;

	if (_color)
	{
		if (ASCII_B <= (int)board.getTile (start.getX (), start.getY ()) && (int)board.getTile (start.getX (), start.getY () <= ASCII_R))
		{
			for (int i = 0; i < compMoves->length (); i += 2)
			{
				compDest.setX ((int)((*compMoves)[i] - 48));
				compDest.setY ((int)((*compMoves)[i + 1] - 48));

				if (compDest.getX () == dest.getX () && compDest.getY () == dest.getY ())
				{
					if (ASCII_B <= board.getTile (dest.getX (), dest.getY ()) && board.getTile (dest.getX (), dest.getY ()) <= ASCII_R)
					{
						delete compMoves;
							return 3;
					}
					else if (board.getTile (dest.getX (), dest.getY ()) == ASCII_k)
					{
						delete compMoves;
						return 1;
					}
					delete compMoves;
					return 0;
				}

			}
		}
		else
		{
			delete compMoves;
			return 2;
		}

	}
	else
	{
		if (ASCII_b <= (int)board.getTile (start.getX (), start.getY ()) && (int)board.getTile (start.getX (), start.getY () <= ASCII_r))
		{
			for (int i = 0; i < compMoves->length (); i += 2)
			{
				compDest.setX ((int)((*compMoves)[i] - 48));
				compDest.setY ((int)((*compMoves)[i + 1] - 48));

				for (int i = 0; i < compMoves->length (); i += 2)
				{
					compDest.setX ((int)((*compMoves)[i] - 48));
					compDest.setY ((int)((*compMoves)[i + 1] - 48));

					if (compDest.getX () == dest.getX () && compDest.getY () == dest.getY ())
					{
						if (ASCII_b <= board.getTile (dest.getX (), dest.getY ()) && board.getTile (dest.getX (), dest.getY ()) <= ASCII_r)
						{
							delete compMoves;
							return 3;
						}
						else if (board.getTile (dest.getX (), dest.getY ()) == ASCII_K)
						{
							delete compMoves;
							return 1;
						}
						delete compMoves;
						return 0;
					}

				}
			}
		}
		else
		{
			delete compMoves;
			return 2;
		}
	}
	delete compMoves;
	return 6;
}

std::string* knight::get_all_moves(point start)
{
	std::string* moves = new std::string;
	Board board;

	for (int i = start.getY () - 2; i <= start.getY () + 2; i++)
	{
		if (i != start.getY ())
		{
			if (i == start.getY () - 2 || i == start.getY () + 2)
			{
				try
				{
					board.getTile (start.getX () - 1, i);
					moves->append (std::to_string (start.getX() - 1) + std::to_string (i));
				}
				catch (...) {}
				try
				{
					board.getTile (start.getX () + 1, i);
					moves->append (std::to_string (start.getX() + 1) + std::to_string (i));
				}
				catch (...) {}
			}
			else if (i == start.getY () - 1 || i == start.getY () + 1)
			{
				try
				{
					board.getTile (start.getX () - 2, i);
					moves->append (std::to_string (start.getX () - 2) + std::to_string (i));
				}
				catch (...) {}
				try
				{
					board.getTile (start.getX () + 2, i);
					moves->append (std::to_string (start.getX () + 2) + std::to_string (i));
				}
				catch (...) {}
			}
		}
	}
}