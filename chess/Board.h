#pragma once
#include "Troop.h"
#include <iostream>;
#include <string>

const int BOARD_SIZE = 8;
class Board
{
private:
	char _gameBoard[BOARD_SIZE][BOARD_SIZE];
public:
	Board();
	void Print()const;
	void getStartString (char* dest);
	std::string Update(std::string newMove, bool currColor);
	char getTile (int x, int y);
};