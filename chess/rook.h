#pragma once
#include <iostream>
#include <string>
#include "troop.h"
#include "Board.h"
class rook : troop
{
public:
	virtual std::string* get_all_moves(point start);
	virtual int move(point start, point dest);
	rook(bool color);
private:
	bool _color;//true for white false for black
};