#pragma once
#include <iostream>
#include <string>
#include "troop.h"
class queen
{
public:
	queen(bool color);
	int move(point start, point dest);
	virtual std::string* get_all_moves(point start);

private:
	bool _color;//true for white false for black
};